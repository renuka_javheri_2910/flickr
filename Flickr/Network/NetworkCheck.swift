//
//  NetworkCheck.swift
//  Flickr
//
//  Created by Renuka Javheri on 05/12/21.
//

import Foundation
import Alamofire

class NetworkCheck{
    class func isReachable() -> Bool{
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    class func isReachableOnWifi() -> Bool{
        return NetworkReachabilityManager()?.isReachableOnEthernetOrWiFi ?? false
    }
}
