//
//  FlickerSearchViewController.swift
//  Flickr
//
//  Created by Renuka Javheri on 05/12/21.
//

import UIKit


class FlickerSearchViewController: UIViewController , UISearchBarDelegate{

    
    @IBOutlet weak var flickrTableView: UITableView!
    @IBOutlet weak var flickrSearchBar: UISearchBar!
    
    var flickerPhotoData = [FlickerPhoto]()
    let cellRowHeight: CGFloat = 150
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        flickrSearchBar.delegate = self
        flickrTableView.dataSource = self
        flickrTableView.delegate = self
        searchForPhotos()
    }
    // MARK:- SearchButton
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar .resignFirstResponder()
        searchForPhotos()
    }
    
    func searchForPhotos(){
        let searchText = flickrSearchBar.text
        if searchText != nil{
           
            FlickrApiService().getDataFromFlickerApi(text: flickrSearchBar.text ?? "", resultType: FlickerResult.self) { response in
                    guard let response = response else {return}
                    let photo = response.photos?.photo ?? []
                    
                    for photoUrlData in photo {
                        let id = photoUrlData.id
                        let farm = photoUrlData.farm
                        let server = photoUrlData.server
                        let secret = photoUrlData.secret
                        
                        let photoData = FlickerPhoto(id: id ?? "", farm: farm ?? 0, secret: secret ?? "", server: server ?? "")
                        self.flickerPhotoData.append(photoData)
                    }
                    DispatchQueue.main.async {
                        self.flickrTableView.reloadData()
                    }
            }
        }else {
        
            AppDel.window?.rootViewController?.view.makeToast(ToastMessage.searchText)
        }
      
    }
}

extension FlickerSearchViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flickerPhotoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FlickrSearchTableViewCell.self)) as! FlickrSearchTableViewCell
        cell.flickrIdLable.text = flickerPhotoData[indexPath.row].id
        
       let photoUrl = flickerPhotoData[indexPath.row].imageURL as! URL
        cell.flickrImageView.downloaded(from: photoUrl)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: String(describing: FlickerDetailsViewController.self)) as! FlickerDetailsViewController
        viewController.flickerPhotoDetail = flickerPhotoData[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellHeight: CGFloat = cellRowHeight
        return cellHeight
    }
    
}
