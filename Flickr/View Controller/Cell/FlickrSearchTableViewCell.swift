//
//  FlickrSearchTableViewCell.swift
//  Flickr
//
//  Created by Renuka Javheri on 05/12/21.
//

import UIKit

class FlickrSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var flickrImageView: UIImageView!
    @IBOutlet weak var flickrIdLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
