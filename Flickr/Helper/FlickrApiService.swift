//
//  FlickrApiService.swift
//  Flickr
//
//  Created by Renuka Javheri on 05/12/21.
//

import Foundation


protocol FlickerApiServiceProtocol{
    func getDataFromFlickerApi<T: Codable>(text: String, resultType: T.Type, completionHandler: @escaping(_ result: T?) -> Void)
}

class FlickrApiService{
    
    func getDataFromFlickerApi<T: Codable>(text: String, resultType: T.Type, completionHandler: @escaping (T?) -> Void) {
        
        guard let flickrUrlPath = FlickerURL().getFlickerUrl(searchText: "\(text)") else { return }
        
        if NetworkCheck.isReachable(){
            print("Connected to Internet")
            URLSession.shared.dataTask(with: flickrUrlPath) { data, respponse, error in
                
                do{
                    guard let data = data else { return }
                    let result = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(result)
                }catch{
                    print("Error in getting data")
                }
            }.resume()
        }else if NetworkCheck.isReachableOnWifi(){
            print("Connected to Wifi")
            URLSession.shared.dataTask(with: flickrUrlPath) { data, respponse, error in
                do{
                    guard let data = data else { return }
                    let result = try JSONDecoder().decode(T.self, from: data)
                    completionHandler(result)
                }catch{
                    print("Error in getting data")
                }
            }.resume()
        }else{
            AppDel.window?.rootViewController?.view.makeToast(ToastMessage.internetError)
        }
    }
}
