//
//  FlickerPhotos.swift
//  Flickr
//
//  Created by Renuka Javheri on 05/12/21.
//

import Foundation

struct FlickerPhoto {
    var id: String
   // var title: String
    var farm: Int
    var secret: String
    var server: String
    var imageURL: NSURL {
        get {
            let url = NSURL(string: "http://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_m.jpg")!
            return url
        }
    }
}
